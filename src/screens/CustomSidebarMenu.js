
import React from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  Image,
  Text,
  Linking,
  
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
  DrawerContentScrollView,
  DrawerItemList
} from '@react-navigation/drawer';

const CustomSidebarMenu = (props) => {

    return (
        <SafeAreaView style={{flex: 1, backgroundColor:"white" }}>
          {/*Top Large Image */}
          <View  style={styles.sideMenuProfileIcon}>
            </View>
            <Text style={styles.LogoText}>Book Search</Text>
          <DrawerContentScrollView {...props}
          >
            <DrawerItemList {...props} />
            
          </DrawerContentScrollView>
          
        </SafeAreaView>
      );
}

const styles = StyleSheet.create({
    sideMenuProfileIcon: {
      resizeMode: 'center',
      width: wp('68%'),
      height: wp('10%'),
     
      alignSelf: 'center',
      top: hp('2%'),
      backgroundColor:"transparent"
    },
    iconStyle: {
      width: 15,
      height: 15,
      marginHorizontal: 5,
    },
    customItem: {
      padding: 16,
      flexDirection: 'row',
      alignItems: 'center',
    },
    LogoText:{
      color:"#8B0000",
      alignSelf: 'center',
      top: hp('2%'),
      fontSize:wp('10%'),
      marginBottom:hp('5%'),
      fontFamily:'Cairo_600SemiBold',
    }
});
  
export default CustomSidebarMenu;