import * as React from 'react';
import { 
  Text, 
  StyleSheet,
  View ,
  TouchableOpacity,
  ScrollView
  } from 'react-native';
import Header from '../../components/header'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import API_URL from '../screens/URL';
import axios from 'axios';
import { Card } from 'react-native-elements'


const Details_Book = ({navigation,route}) => {
  const[loaded,setloaded]=React.useState(false);
  const[ISBN,setISBN]=React.useState(route.params.ISBN);
  const[title,setTitle]=React.useState('');
  const[data,setData]=React.useState('');
  const[numberofpages,setNumberofpages]=React.useState('');
  const[isbn_13,setisbn_13]=React.useState('');
  const[publishers,setPublishers]=React.useState('');

        async function getData() {
          console.log(route.params.ISBN);
          fetch(API_URL+`api/books?bibkeys=ISBN:${route.params.ISBN}&jscmd=data&format=json`, {
            method: 'GET',
       
            }).then((response) => response.json())
                .then((responseJson) => {

              console.log(responseJson);
              console.log(`"ISBN:${route.params.ISBN}"`);
              let obj = `ISBN:${route.params.ISBN}`
              console.log(responseJson[`${obj}`]);
            
              setData(responseJson[`${obj}`])
              setloaded(true)
            }).catch((error) => {
              console.error(error);


        
        });
      }
    

      React.useEffect(() => {
        console.log(route.params.ISBN);

              getData();
            }, []);                    
         
          
          
    return (

      <>
  

      <Header/>
     
      <View style={{flex:2,backgroundColor:'#fff',marginTop:hp('-14%'),borderTopLeftRadius:50,borderTopRightRadius:50}}>
      <ScrollView>


                                                {/*More Details of book */}
{(loaded == true)?

    <Card containerStyle={styles.cardCnt}  >
            <View style={{backgroundColor:'#fff',marginTop:hp('-2%'), fontFamily:'Cairo_600SemiBold'}}> 
                    <Card.Title style={{color:'#8B0000' , fontFamily:'Cairo_600SemiBold',fontSize:wp('5%'),marginTop:hp('2%')}}>Details of the book</Card.Title>
                    </View>
                            <Card.Divider/>

                                    <Text style={styles.cardtext}>
                                   Title Name: <Text style={styles.cardtextblack}>{route.params.title}</Text>
                                    </Text>
                                    <Card.Divider/>   
                                    <Text style={styles.cardtext}>
                                    Authors: <Text style={styles.cardtextblack}>{data.authors[0].name}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                    Subjects: <Text style={styles.cardtextblack}>{data.subjects[0].name}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                    URL: <Text style={styles.cardtextblack}>{data.authors[0].url}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                    ISPN: <Text style={styles.cardtextblack}>{route.params.ISBN}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                    Number of pages: <Text style={styles.cardtextblack}>{route.params.numberofpages}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                    Publishers: <Text style={styles.cardtextblack}>{route.params.publisher}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                    Publish Date: <Text style={styles.cardtextblack}>{route.params.date}</Text>
                                    </Text>
                                    <Card.Divider/>
                            
           
                </Card>
              
  :
  
  <Text>{''}</Text>
  
  
  }

  </ScrollView>
      </View>
    

      </>
    );
  }
  const styles = StyleSheet.create({

    texttitle:{
      fontSize: wp('7%'),
        textAlign: 'center',
        top:hp('2%'),
        color: "#8B0000",
        marginBottom: hp('4%'),
  },
cardtext:{
   marginBottom: 10,
   color:'#8B0000',
   fontSize:wp('5%')
},
cardtextblack:{
  marginBottom: 10,
  color:'#000',
  fontSize:wp('5%')
},
  inputStyle1: {
    
    fontFamily:'Cairo-SemiBold',
    marginTop:hp('4%'),
    fontSize: wp('6%'),
    borderRadius:10,
    alignSelf:"center",
    color: '#000000',
    width:wp('90%'),
    height:hp('7%'),
    backgroundColor:"#fafafc",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity:50,
    shadowRadius: 60,
    elevation: 20,
    textAlign:"center",
  },

  buttonSearch:{
     backgroundColor: "#8B0000",
     borderColor:'#8B0000',
     padding:wp('2%'),
     fontFamily:'Cairo-SemiBold',
     borderRadius:10,
     borderWidth: wp('0.3%'),
     width:wp('68%'),
     height:52,
     alignSelf:'center',
     marginTop:hp('4%'),
     shadowColor: "#000",
     shadowOffset: {
       width: 20,
       height: 10,
     },
     shadowOpacity:50,
     shadowRadius: 60,
     elevation: 20,
    textAlign:"center",

  },
  buttonDetails:{
    backgroundColor: "#8B0000",
    borderColor:'#8B0000',
    padding:wp('2%'),
    fontFamily:'Cairo-SemiBold',
    borderRadius:10,
    borderWidth: wp('0.3%'),
    width:wp('68%'),
    height:52,
    alignSelf:'center',
    marginTop:hp('1%'),
    shadowColor: "#000",
    shadowOffset: {
      width: 20,
      height: 10,
    },
    shadowOpacity:50,
    shadowRadius: 60,
    elevation: 20,
   textAlign:"center",

 },
  buttontxtSearch: {   
    color: "#fff",
    fontSize: wp('7%'),
    textAlign: "center",
    fontFamily:'Cairo-SemiBold',
    alignSelf:'center',
    marginTop:hp('-1%'),
  },

  cardCnt: {
    borderWidth: 1, // Remove Border
    shadowColor: "#000", // Remove Shadow IOS
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 1,
    shadowRadius: 1,
    elevation: 1, // This is for Android
    backgroundColor:'#fff',
    color: "#8B0000",
   
  },
    });
  export default Details_Book;