import * as React from 'react';
import { 
  Text, 
  StyleSheet,
  View ,
  TextInput,
  TouchableOpacity,
  FlatList,
  ScrollView
  } from 'react-native';
import Header from '../../components/header'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import API_URL from '../screens/URL';
import axios from 'axios';
import { Card } from 'react-native-elements'
import Toast from 'react-native-tiny-toast';


const Search_Book_ISBN = ({navigation}) => {
  const[flag,setFlag]=React.useState(false);
  const[data,setdata]=React.useState([]);
  const[ISBN,setISBN]=React.useState('');

  const onPress = (value) => {
    const regex =/^[0-9]*$/
    if(ISBN.match(regex) === null){
      Toast.show('Please Enter Numbers Only',{
        containerStyle:{backgroundColor:"red"},
        textStyle: {fontSize:19},
        mask: true,
        maskStyle:{},
        
        })
    }
  
     console.log('value',value);
     setISBN(value)
                              }

                              async function Search() {
                                let books = data
                                const regex =/^[0-9]*$/
                                  if(ISBN == '' || ISBN.match(regex) === null){
                                    setFlag(false)
                                    Toast.show('Please Enter Valid ISPN',{
                                      position:660,
                                      containerStyle:{backgroundColor:"red"},
                                      textStyle: {fontSize:19},
                                      mask: true,
                                      maskStyle:{},
                                      
                                      })
                                  
                                  }else{
                                   
                                try{
                                  const response = await axios.get(
                                    API_URL+`isbn/${ISBN}.json`
                                  )

                                  
                                   let book ={

                                      title:response.data.title,
                                      date:response.data.publish_date,
                                      numberofpages:response.data.number_of_pages,
                                      publishers:response.data.publishers,
                                      isbn_13:response.data.isbn_13,
                                   }
                                    books.push(book)
                                    setFlag(true)
                                    setdata(books)
             
                                  }
                                
                                catch(e)
                                {
                                  setFlag(false)
                                  Toast.show('NO ISPN with this number',{
                                    position:660,
                                    containerStyle:{backgroundColor:"red"},
                                    textStyle: {fontSize:19},
                                    mask: true,
                                    maskStyle:{},
                                    
                                    })
                                }
                              }
                              }
                              const renderitems = ({item}) => {
                              
                                 return(
                                   <>
                                      <Card containerStyle={styles.cardCnt}  >
            <View style={{backgroundColor:'#fff',marginTop:hp('-2%'), fontFamily:'Cairo_600SemiBold'}}> 
                    <Card.Title style={{color:'#8B0000' , fontFamily:'Cairo_600SemiBold',fontSize:wp('5%'),marginTop:hp('2%')}}>Details of the book</Card.Title>
                    </View>
                                    
                                             <Text style={styles.cardtext}>
                                   Title Name: <Text style={styles.cardtextblack}>{item.title}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                   ISPN: <Text style={styles.cardtextblack}>{item.isbn_13}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                   Number of pages: <Text style={styles.cardtextblack}>{item.numberofpages}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                   Publishers: <Text style={styles.cardtextblack}>{item.publishers}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <Text style={styles.cardtext}>
                                   Publish Date: <Text style={styles.cardtextblack}>{item.date}</Text>
                                    </Text>
                                    <Card.Divider/>
                                    <TouchableOpacity style={styles.buttonDetails} onPress={() =>navigation.navigate('Book Details',{ISBN:ISBN,title:item.title,numberofpages:item.numberofpages,publisher:item.publishers,date:item.date,})} >
                                     <Text style={styles.buttontxtSearch}>More Details</Text>
                                   </TouchableOpacity>
           
                                        </Card>
                                          
                                   </>
                                 )
                               }      
                            
    return (
      <>
  

      <Header/>
     
      <View style={{flex:2,backgroundColor:'#fff',marginTop:hp('-14%'),borderTopLeftRadius:50,borderTopRightRadius:50}}>
      <ScrollView>
      <Text style={styles.texttitle}>
        Search by book's ISBN
      </Text>
      <TextInput
      placeholder="Please enter ISBN"
      value={ISBN}
      style={styles.inputStyle1} 
      keyboardType='numeric'
      textAlign='center'
      placeholderTextColor = "#8B0000"
      onChangeText={(text) => onPress(text)}
      />


<TouchableOpacity style={styles.buttonSearch}       onPress={() => Search() } >
         <Text style={styles.buttontxtSearch}>Search</Text>
      </TouchableOpacity>


                                                {/* Details of book */}


    {(flag == false)?
      
    <Text>{''}</Text>
    :
    <View style={{flex: 1, padding: 16, marginTop:12}}>
    <FlatList
    data={data}
    renderItem={renderitems}
    keyExtractor={item => item.isbn_13}
  />
  </View>
              }
  

  </ScrollView>
      </View>
    

      </>
    );
  }
  const styles = StyleSheet.create({

    texttitle:{
      fontSize: wp('7%'),
        textAlign: 'center',
        top:hp('2%'),
        color: "#8B0000",
        marginBottom: hp('4%'),
  },
cardtext:{
   marginBottom: 10,
   color:'#8B0000',
   fontSize:wp('5%')
},
cardtextblack:{
  marginBottom: 10,
  color:'#000',
  fontSize:wp('5%')
},
  inputStyle1: {
    
    fontFamily:'Cairo-SemiBold',
    marginTop:hp('4%'),
    fontSize: wp('6%'),
    borderRadius:10,
    alignSelf:"center",
    color: '#000000',
    width:wp('90%'),
    height:hp('7%'),
    backgroundColor:"#fafafc",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity:50,
    shadowRadius: 60,
    elevation: 20,
    textAlign:"center",
  },

  buttonSearch:{
     backgroundColor: "#8B0000",
     borderColor:'#8B0000',
     padding:wp('2%'),
     fontFamily:'Cairo-SemiBold',
     borderRadius:10,
     borderWidth: wp('0.3%'),
     width:wp('68%'),
     height:52,
     alignSelf:'center',
     marginTop:hp('4%'),
     shadowColor: "#000",
     shadowOffset: {
       width: 20,
       height: 10,
     },
     shadowOpacity:50,
     shadowRadius: 60,
     elevation: 20,
    textAlign:"center",

  },
  buttonDetails:{
    backgroundColor: "#8B0000",
    borderColor:'#8B0000',
    padding:wp('2%'),
    fontFamily:'Cairo-SemiBold',
    borderRadius:10,
    borderWidth: wp('0.3%'),
    width:wp('68%'),
    height:52,
    alignSelf:'center',
    marginTop:hp('1%'),
    shadowColor: "#000",
    shadowOffset: {
      width: 20,
      height: 10,
    },
    shadowOpacity:50,
    shadowRadius: 60,
    elevation: 20,
   textAlign:"center",

 },
  buttontxtSearch: {   
    color: "#fff",
    fontSize: wp('7%'),
    textAlign: "center",
    fontFamily:'Cairo-SemiBold',
    alignSelf:'center',
    marginTop:hp('-1%'),
  },

  cardCnt: {
    borderWidth: 1, // Remove Border
    shadowColor: "#000", // Remove Shadow IOS
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 1,
    shadowRadius: 1,
    elevation: 1, // This is for Android
    backgroundColor:'#fff',
    color: "#8B0000",
   
  },
    });
  export default Search_Book_ISBN;