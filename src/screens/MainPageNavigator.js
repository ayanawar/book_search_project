import * as React from 'react';
import { TouchableOpacity, View } from 'react-native';
/////////// Main Navigation //////
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
////////// Drawer Style /////////
import CustomSidebarMenu from './CustomSidebarMenu';
/////// Screens /////////
import Search_Book_ISBN from '../screens/Search_Book_ISBN';
import Details_Book from '../screens/Details_Book';
///////icons//////////////
import Feather from 'react-native-vector-icons/Feather';
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const NavigationDrawerStructure = (props) => {
    //Structure for the navigatin Drawer
    const toggleDrawer = () => {
      //Props to open/close the drawer
      props.navigationProps.toggleDrawer();
    };
  
    return (
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity onPress={toggleDrawer}>
          {/*Donute Button Image */}
          <View style={{marginTop: 10, marginLeft: 10}}>
          <Feather
                name="menu"
                size={26}
                color="#fff"
          />
          </View>
        </TouchableOpacity>
      </View>
    );
};


function Search_Book_ISBN_stack({navigation}) {
    return (
        <Stack.Navigator
          initialRouteName="Search_Book_ISBN"
          screenOptions={{
            headerLeft: () => (
              <NavigationDrawerStructure navigationProps={navigation} />
            ),
            headerStyle: {
              backgroundColor: '#8B0000', //Set Header color
            },
            headerTintColor: '#fff', //Set Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Set Header text style
            },
          }}>
          
          <Stack.Screen
            name="Search For Books"
            component={Search_Book_ISBN}
          />

          <Stack.Screen
            name="Book Details"
            component={Details_Book}
          />


        </Stack.Navigator>
    )
    }
  

const MainPageNavigator = (props) => {
  return (
    <NavigationContainer>
         <Drawer.Navigator
          drawerContentOptions={{
            itemStyle: {marginVertical: 5, fontSize:'20%',fontFamily:'Cairo-SemiBold',},
            activeTintColor: '#ffffff',
            inactiveBackgroundColor:'#ffffff',
            activeBackgroundColor:'#8B0000',
            inactiveTintColor:'#8B0000',
            labelStyle: {
              fontSize:19,
              fontFamily:'Cairo-SemiBold',
            },
  
          }}
          drawerContent={(props) => <CustomSidebarMenu {...props} />}

          
           initialRouteName="Home">
        <Drawer.Screen name="Search" component={Search_Book_ISBN_stack} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

export default MainPageNavigator;